'use strict';

/**
 * An asynchronous bootstrap function that runs before
 * your application gets started.
 *
 * This gives you an opportunity to set up your data model,
 * run jobs, or perform some special logic.
 *
 * See more details here: https://strapi.io/documentation/v3.x/concepts/configurations.html#bootstrap
 */

module.exports = async () => {
    // Some chunks aren not documentated, found some impl on stackoverflow instead
    const admins = await strapi.query('user', 'admin').find({ _limit: 1 });
    if (admins.length == 0) {
        console.log('Creating admin user');
        try {
            const params = {
                username: 'strapi',
                password: 'strapi',
                firstname: 'Strapi',
                lastname: 'Backend',
                email: 'dev@gmail.com',
                blocked: false,
                isActive: true,
            }
            // in the admin under services you can find the super admin role
            // On fresh start, you'll probably need to restart your sever as this role will not exist
            let superAdminRole = await strapi.admin.services.role.getSuperAdmin();

            // Strapi may start up before DB, we'll just wait a bit and call again
            if (!superAdminRole) {
                await new Promise(r => setTimeout(r, 2000));
                superAdminRole = await strapi.admin.services.role.getSuperAdmin();
            }

            // Hash password before storing in the database
            params.roles = [superAdminRole.id]
            params.password = await strapi.admin.services.auth.hashPassword(params.password);
    
            // Create admin account
            const admin = await strapi.query('user', 'admin').create({
                ...params
            });
    
            console.info('(Admin) Account created:', admin);
    
        } catch (error) {
            console.error(error);
        }
    } else {
        console.log("Admin already exists")
    }
};
