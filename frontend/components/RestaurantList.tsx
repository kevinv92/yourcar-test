
import { gql, useQuery } from '@apollo/client';
import Link from 'next/link';
import Table from 'react-bootstrap/Table'
import { Restaurant, CuisineType } from '../types/backendTypes';

const GET_RESTAURANTS = gql`
query GetRestaurants($filter: [String!]) {
    restaurants (
      where: {
        cuisine_types: {
          name_in: $filter
        }
      }
    ) {
      id
      name
      cuisine_types {
        name
      }
    }
}
`;

function RestaurantItem({ name, id, cuisine_types }: Restaurant) {
    const cuisineTypeString = cuisine_types?.map(({ name }: CuisineType) => name)
        .join(',') || 'Unknown';
    return (
        <Link href={`/restaurant/${id}`}>
            <tr>
                <td>{ name }</td>
                <td>{ cuisineTypeString }</td>
            </tr>
        </Link>
    );
};

interface RestaurantListFilter {
    cuisineFilter?: [string]
}

export default function RestaurantList({ cuisineFilter }: RestaurantListFilter) {
    const { data, loading, error } = useQuery(GET_RESTAURANTS, {
        variables: { filter: cuisineFilter }
    });

    if (loading) return <div>Loading...</div>;
    if (error) {
        return (<div>{ JSON.stringify(error) }</div>)
    };
    console.log(data);
    const rows = data.restaurants
        .map((item: Restaurant, idx: number) => <RestaurantItem key={idx} { ...item } />);

    return (
        <Table hover borderless>
            <thead>
                <tr>
                    <th>Name</th>
                    <th>Cuisine Types</th>
                </tr>
            </thead>
            <tbody>
                { rows }
            </tbody>
        </Table>
    )
}