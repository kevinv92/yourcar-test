import React, { FormEvent, ChangeEvent, useState } from 'react';
import { Container, Form, Row, Col, Button  } from 'react-bootstrap';
import { gql, useMutation } from '@apollo/client';

const CREATE_DISH = gql`
mutation CreateDish(
    $name: String!, 
    $price: Float!,
    $desc: String!,
    $restaurantId: ID!,
){
    createDish(input: {
        data: {
            name: $name
            price: $price
            description: $desc
            restaurant: $restaurantId
        }
    }) {
        dish {
            name
            description
            price
        }
    }
}
`;

interface DishFormProps {
    restaurantId: number
    onAction?: () => void
};

export default function DishForm({ restaurantId, onAction }: DishFormProps) {

    const [name, setName] = useState("");
    const [desc, setDesc] = useState("");
    const [price, setPrice] = useState(0);
    const [createDish, { loading, error }] = useMutation(CREATE_DISH);

    const submitEvent = (evt: FormEvent<HTMLFormElement>) => {
        evt.preventDefault();
        createDish({ 
            variables: {
                name, desc, price, restaurantId
            }
        });
        onAction && onAction();
    }

    const handleNameChange = (evt: ChangeEvent<HTMLInputElement>) => setName(evt.target.value);
    const handDescChange = (evt: ChangeEvent<HTMLInputElement>) =>  setDesc(evt.target.value);
    const handlePriceChange = (evt: ChangeEvent<HTMLInputElement>) => setPrice(parseInt(evt.target.value));

    if (error) return <Container>{ JSON.stringify(error) }</Container>
    if (loading) return <Container>{ JSON.stringify(error) }</Container>
    return (
        <Container>
            Add Dish to Restaurant
            <Form onSubmit={submitEvent}>
                <Row>
                    <Col>
                        <Form.Control type="text" placeholder="name" onChange={handleNameChange} />
                    </Col>
                    <Col>
                        <Form.Control type="text" placeholder="description" onChange={handDescChange} />
                    </Col>
                    <Col>
                        <Form.Control type="text" placeholder="price" onChange={handlePriceChange}/>
                    </Col>
                    <Col>
                        <Button type="submit">
                            Submit
                        </Button>
                    </Col>
                </Row>
            </Form>
        </Container>
    );
}