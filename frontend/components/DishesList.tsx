import { Dish } from '../types/backendTypes';
import { Table } from 'react-bootstrap';

interface DishesListProps {
    dishes?: [Dish]
}

export default function DishesList({ dishes }: DishesListProps) {
    const headers = (
        <tr>
            <th>Name</th>
            <th></th>
            <th align="right">Price</th>
        </tr>
    );
    const rows = dishes?.map(({ name, description, price }, idx: number) => {
        return (
            <tr key={idx}>
                <td>{ name }</td>
                <td>{ description }</td>
                <td>{ price }</td>
            </tr>
        )
    });
    return(
        <Table borderless>
            <thead>
                { headers }
            </thead>
            <tbody>
                { rows }
            </tbody>
        </Table>
    )
}
