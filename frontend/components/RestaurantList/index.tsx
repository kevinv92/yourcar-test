
import { gql, useQuery } from '@apollo/client';
import Link from 'next/link';
import Table from 'react-bootstrap/Table'

// TODO: Put somewhere reusable
interface CuisineType {
    name: string
}
interface Restaurant {
    id?: number,
    name?: string
    cuisine_types?: [CuisineType]
}

const GET_RESTAURANTS = gql`
  query GetRestaurants {
    restaurants {
      id
      name
      cuisine_types {
        name
      }
    }
  }
`;

function RestaurantItem({ name, id, cuisine_types }: Restaurant) {
    const cuisineTypeString = cuisine_types?.map(({ name }: CuisineType) => name)
        .join(',') || 'Unknown';
    return (
        <Link href={`/restaurant/${id}`}>
            <tr>
                <td>{ name }</td>
                <td>{ cuisineTypeString }</td>
            </tr>
        </Link>
    );
};

export default function RestaurantList() {
    const { data, loading, error } = useQuery(GET_RESTAURANTS);

    if (loading) return <div>Loading...</div>;
    if (error) {
        return (<div>{ JSON.stringify(error) }</div>)
    };

    const rows = data.restaurants.map((item: Restaurant, idx: number) => 
        <RestaurantItem key={idx} { ...item } />);
    return (
        <Table hover borderless>
            <thead>
                <tr>
                    <th>Name</th>
                    <th>Cuisine Types</th>
                </tr>
            </thead>
            <tbody>
                { rows }
            </tbody>
            {/* { data.restaurants.map(({ name }: Restaurant, idx: string) => <li key={idx}>{ name }</li>) } */}
        </Table>
    )

}