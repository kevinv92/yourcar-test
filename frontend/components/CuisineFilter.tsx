import { Form, Row, Button, Col } from 'react-bootstrap';
import { useState, ChangeEvent, FormEvent } from 'react';

interface CuisineFilterProps {
    acceptFilter: (arg0: string) => void
}
  
export default function CuisineFilter({ acceptFilter }: CuisineFilterProps) {
    const [filter, setFilter] = useState("");
    const onChange = (evt: ChangeEvent<HTMLInputElement>) => setFilter(evt.target.value)
    const doSubmit = (evt: FormEvent<HTMLFormElement>) => {
        evt.preventDefault();
        acceptFilter(filter);
    }

    return (
        <Form onSubmit={ doSubmit }>
        <Row>
            <Col>
                <Form.Control type="text" placeholder="filter cuisine type" onChange={ onChange } />
            </Col>
            <Col>
            <Button type="submit">
                Filter
            </Button>
            </Col>
        </Row>
        </Form>
    );
}