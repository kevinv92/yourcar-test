import { Container, Spinner } from 'react-bootstrap';

export default function Loading() {
    return (<Container><Spinner animation="border" /> Loading ... </Container>)
}