import Head from 'next/head'
import Container from 'react-bootstrap/Container';
import { gql, useQuery } from '@apollo/client';
import { useState } from 'react';

import { CuisineType } from '../types/backendTypes';
import Loading from '../components/Loading';
import RestaurantList from '../components/RestaurantList';
import CuisineFilter from '../components/CuisineFilter';

const ALL_CUISINETYPES = gql`
  query AllCuisineTypes {
    cuisineTypes {
      name
    }
  }
`;

export default function Home() {
  const { data, loading, error } = useQuery(ALL_CUISINETYPES);
  const allCuisineTypes = loading || !data
    ? []
    : data.cuisineTypes.map(({ name }: CuisineType) => name);
  const [filter, setFilter] = useState(allCuisineTypes);
  const acceptFilter = (newFilter: string) => {
    newFilter === "" 
      ? setFilter(allCuisineTypes) 
      : setFilter([newFilter])
  };

  return (
    <div>
      <Head>
        <title>Restaurants</title>
        <link rel="icon" href="/favicon.ico" />
      </Head>

      <Container>
        <div className="title-card">
          <h1>
            Restaurants of the World
          </h1>
          <p>List of restaurants all around the world, with their top dishes </p>
        </div>
        {
          loading 
          ? (<Loading />)
          : (<><CuisineFilter acceptFilter={ acceptFilter }/><RestaurantList cuisineFilter={filter}/></>)
        }
      </Container>
    </div>
  )
}
