import { gql, useQuery } from '@apollo/client';
import { Container, Table, Button, Alert } from 'react-bootstrap';
import { useState } from 'react';
import { Restaurant as IRestaurant } from '../../types/backendTypes';
import client from '../../lib/apolloClient';
import DishForm from '../../components/DishForm';
import DishesList from '../../components/DishesList';
import Loading from '../../components/Loading';

const GET_RESTAURANT = gql`
query GetRestaurant($id: ID!, $limit: Int) {
    restaurant(id: $id) {
        id
        name
        description
        dishes(limit: $limit) {
            name
            description
            price
        }
    }
}
`;

interface RestaurantProps {
    id: number
}


export default function Restaurant({ id }: RestaurantProps) {
    const [limit, setLimit] = useState(5);
    const { data, loading, error, refetch } = useQuery(GET_RESTAURANT, {
        variables: { id, limit }
    })

    if (loading) return (<Loading />)

    const { name, description, dishes }: IRestaurant = data.restaurant;
    return (
        <Container>
            <h2>{ name }</h2>
            <dd>{ description }</dd>
            <DishesList dishes={dishes} />
            <Button onClick={() => setLimit(limit + 5) } >More results</Button>
            <Container>
                <DishForm restaurantId={id} onAction={ () => { 
                    // Puts some delay to make sure the server applies change
                    // This is just a workaround :P, apollo too complicated to learn in a few hours
                    // in a couple of days. Ideally, I'll be updating cache or probably using useLazyQuery
                    // in some manner
                    setTimeout(() => refetch(), 200)} 
                   }/>
            </Container>
            { error && (<Alert variant="danger">{ JSON.stringify(error) }</Alert>)}
        </Container>
    );
}

export async function getStaticPaths() {
    const { data } = await client.query({ 
        query: gql`{ restaurants { id } }`
    });
    const paths = data.restaurants.map(({ id }: { id: number }) => ({
        params: {
            id
        }
    }));
    return {
        paths,
        fallback: false
    };
}

export async function getStaticProps({ params }: any) {
    return {
        props: {
            id: params.id
        }
    }
}