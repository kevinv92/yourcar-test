import { ApolloClient, InMemoryCache } from '@apollo/client';
import { offsetLimitPagination } from '@apollo/client/utilities';

const client = new ApolloClient({
    uri: 'http://localhost:1337/graphql',
    cache: new InMemoryCache({
        typePolicies: {
            Query: {
                fields: {
                    dish: offsetLimitPagination()
                }
            }
        }
    })
});

export default client;