interface CuisineType {
    name: string
}

interface Restaurant {
    id?: number,
    name?: string,
    description?: string,
    cuisine_types?: [CuisineType],
    dishes?: [Dish]
}

interface Dish {
    name: string,
    description: string,
    price: number
}

export type {
    CuisineType,
    Restaurant,
    Dish
}
